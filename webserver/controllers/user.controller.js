const User = require("../models/user.model.js")

// Créer et enregistrer un nouvel utilisateur
exports.create = (req, res) => {
  // Valider la requête
  if (!req.body) {
    res.status(400).send({
      message: "Le contenu ne peut pas être vide !"
    })
  }

  // Créer un utilisateur
  const user = new User({
    firstname: req.body.firstname,
    lastname: req.body.lastname,
    age: req.body.age
  })

  // Enregistrer l'utilisateur dans la base
  User.create(user, (err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Erreur lors de la création de l'utilisateur."
      })
    else res.send(data);
  })  
}

// Récupérer tous les utilisateurs de la base de données
exports.findAll = (req, res) => {
    User.getAll((err, data) => {
        if (err)
          res.status(500).send({
            message:
              err.message || "Erreur lors de la récupération des utilisateurs."
          })
        else res.send(data)
      })
}

// Récupérer un utilisateur en fonction de son id
exports.findOne = (req, res) => {
    User.findById(req.params.userId, (err, data) => {
        if (err) {
          if (err.kind === "not_found") {
            res.status(404).send({
              message: `Utilisateur introuvable avec l'id ${req.params.userId}.`
            })
          } else {
            res.status(500).send({
              message: "Erreur lors de la récupération de l'utilisateur avec l'id " + req.params.userId
            })
          }
        } else res.send(data)
      })  
}

// Modifier un utilisateur en fonction de son id
exports.update = (req, res) => {
  // Valider la requête
  if (!req.body) {
    res.status(400).send({
      message: "Le contenu ne peut pas être vide"
    })
  }

  User.updateById(
    req.params.userId,
    new User(req.body),
    (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Utilisateur introuvable avec l'id ${req.params.userId}.`
          })
        } else {
          res.status(500).send({
            message: "Erreur lors de la modification de l'utilisateur avec l'id " + req.params.userId
          })
        }
      } else res.send(data)
    }
  )  
}

// Supprimer un utilisateur en fonction de son id
exports.delete = (req, res) => {
    User.remove(req.params.userId, err => {
        if (err) {
          if (err.kind === "not_found") {
            res.status(404).send({
              message: `Utilisateur introuvable avec l'id ${req.params.userId}.`
            })
          } else {
            res.status(500).send({
              message: "Erreur lors de la suppression de l'utilisateur avec l'id " + req.params.userId
            })
          }
        } else res.send({ message: `Utilisateur supprimé avec succès !` })
      })  
}

// Supprimer tous les utilisateurs de la base de données
exports.deleteAll = (req, res) => {
    User.removeAll(err => {
        if (err)
          res.status(500).send({
            message:
              err.message || "Erreur rencontrée lors de la suppression de tous les utilisateurs"
          });
        else res.send({ message: `Tous les utilisateurs ont été supprimés avec succès !` });
      });  
}