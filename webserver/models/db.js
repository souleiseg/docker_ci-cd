const mysql = require("mysql")
const dbConfig = require("../config/db.config.js")

// Créer une connexion à la bdd
const connection = mysql.createConnection({
    host: dbConfig.HOST,
    user: dbConfig.USER,
    password: dbConfig.PASSWORD,
    database: dbConfig.DB
})

// Démarrer la connexion MySQL
connection.connect(error => {
    if (error) {
        throw error
    }
    console.log("Connexion à la base de données réussie !")
})

module.exports = connection