const sql = require("./db.js")

// Constructeur
const User = function(user) {
    this.firstname = user.firstname
    this.lastname = user.lastname
    this.age = user.age
}

User.create = (newUser, result) => {
    sql.query("INSERT INTO users SET ?", newUser, (err, res) => {
        if (err) {
            console.log("error: ", err)
            result(err, null)
            return
        }
        console.log("Utilisateur créé: ", { id: res.insertId, ...newUser })
        result(null, { id: res.insertId, ...newUser })
    })
}

User.findById = (userId, result) => {
    sql.query(`SELECT * FROM users WHERE id = ${userId}`, (err, res) => {
        if (err) {
            console.log("error: ", err)
            result(err, null)
            return
        }

        if (res.length) {
            console.log("Utilisateur trouvé: ", res[0])
            result(null, res[0])
            return
        }

        // Utilisateur introuvable via l'id
        result({ kind: "not_found" }, null)
    })
}

User.getAll = result => {
    sql.query("SELECT * FROM users", (err, res) => {
        if (err) {
            console.log("error: ", err)
            result(null, err)
            return;
        }
        console.log("Utilisateurs: ", res)
        result(null, res)
    })
}

User.updateById = (id, user, result) => {
    sql.query(
      "UPDATE users SET firstname = ?, lastname = ?, age = ? WHERE id = ?",
      [user.firstname, user.lastname, user.age, id],
      (err, res) => {
        if (err) {
          console.log("error: ", err)
          result(null, err)
          return
        }
  
        if (res.affectedRows == 0) {
          // Utilisateur introuvable via l'id
          result({ kind: "not_found" }, null)
          return
        }
  
        console.log("Utilisateur mis à jours: ", { id: id, ...user })
        result(null, { id: id, ...user })
      }
    )
  }

User.remove = (id, result) => {
    sql.query("DELETE FROM users WHERE id = ?", id, (err, res) => {
        if (err) {
            console.log("error: ", err)
            result(null, err)
            return
        }

        if (res.affectedRows == 0) {
            // Utilisateur introuvable via l'id
            result({ kind: "not_found" }, null)
            return
        }

        console.log("Utilisateur supprimé portant l'id: ", id)
        result(null, res)
    })
}

User.removeAll = result => {
    sql.query("DELETE * FROM users", (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
  
      console.log(`deleted ${res.affectedRows} users`);
      result(null, res);
    });
  };
  
  module.exports = User;