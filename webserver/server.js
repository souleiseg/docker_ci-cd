// Express permet de mettre en place les apis REST
// Body-Parser aide à analyser les requête et créer l'object req.body nécessaire pour accéder aux routes
const express = require("express")
const bodyParser = require("body-parser")

const app = express()

// Analyse les requêtes dont le contenu est de type: application/json
app.use(bodyParser.json())

// Analyse les requêtes dont le contenu est de type: application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true}))

// Simple route
app.get("/", (req, res) => {
    res.json({ message: "Bienvenue sur notre appli web !" })
})

require("./routes/user.routes.js")(app)

// Configuration du port d'écoute pour les requêtes
app.listen(3000, () => {
    console.log("Le serveur tourne sur le port 3000")
})