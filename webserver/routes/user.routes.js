module.exports = app => {
    const users = require("../controllers/user.controller.js")
  
    // Créer un nouvel utilisateur
    app.post("/users", users.create)
  
    // Accéder à tous les utilisateurs
    app.get("/users", users.findAll)
  
    // Accéder à un utilisateur en fonction de son id
    app.get("/users/:userId", users.findOne)
  
    // Modifier un utilisateur en fonction de son id
    app.put("/users/:userId", users.update)

    // Supprimer un utilisateur en fonction de son id
    app.delete("/users/:userId", users.delete)
  
    // Supprimer tous les utilisateurs
    app.delete("/users", users.deleteAll)
  }