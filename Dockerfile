FROM node:alpine
EXPOSE 3000/tcp
WORKDIR /webserver
COPY ./webserver .      
RUN npm install  
ENTRYPOINT [ "npm" ]
CMD [ "start" ]
