# Documentation du TP Pipeline Intégration
Par Benoit Galmot, Souleimane Seghir et Florian Lafuente

## Présentation de la solution
### Objectif
Notre objectif était d'avoir deux containers docker qui peuvent communiquer entre eux et qui sont déployés sur un serveur distant.

Nous avons décidé d'utiliser les containers suivants : un pour une application web (en Node) et l'autre pour la base de données (MySQL).

### Pipeline d'intégration
La pipeline d'intégration de l'application web est décomposé en 4 étapes : 
- 'lint', pour identifier les erreurs de syntaxe dans le code
- 'test', pour vérifier que nos tests unitaires passent bien et que nos modifications n'ont pas d'effets de bord
- 'build', pour créer une image docker sur le registry de gitlab
- 'deploy', pour déployer l'application sur un serveur distant (accessible à tous ou juste à l'équipe de développement selon les besoins)

Cette pipeline nous permet de vérifier la qualité de notre code et de facilement déployer notre application.

## Installation brève de la solution
### Prérequis

- Cloner les repository git:
```
$ git clone git@gitlab.com:souleiseg/docker_ci-cd_db.git
$ git clone git@gitlab.com:souleiseg/docker_ci-cd.git
```

- Docker
    • Win 10: https://docs.docker.com/docker-for-windows/install/
    • Autre: https://docs.docker.com/engine/install/
- NodeJS


### Configuration du projet web
- Dockerfile : 
```(dockerfile)
FROM node:alpine
EXPOSE 3000/tcp
WORKDIR /webserver
COPY ./webserver .      
RUN npm install  
ENTRYPOINT [ "npm" ]
CMD [ "start" ]
```
Ce Dockerfile va permettre d'ouvrir le port 3000, de copier les sources du projet et d'installer ses dépendances. L'application démarrera au lancement du conteneur (`npm start`).

- Fichier `.gitlab-ci.yml` :
```(yml)
image: docker:18

stages:
  - lint
  - test
  - build
  # On commente le stge suivant car le raspberry n'est pas connecté
  #- deploy_staging

services:
  - docker:dind

before_script:
  - echo -n $CI_JOB_TOKEN | docker login -u gitlab-ci-token --password-stdin $CI_REGISTRY
  - mkdir -p ~/.ssh
  - apk update
  - apk add nodejs npm
  # Setup SSH deploy keys
  - 'which ssh-agent || ( apk add openssh-client )'
  - eval $(ssh-agent -s)
  - echo "$SSH_PRIVATE_KEY" | tr -d '\r' > ~/.ssh/id_rsa
  - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'

lint:
  stage: lint
  script:
    - cd ./webserver
    - npm install
    - npm run lint

test:
  stage: test
  script:
    - cd ./webserver
    - npm install
    - npm run test


Build:
  stage: build
  script:
    - docker build -t $CI_REGISTRY_IMAGE:web ./
    - docker push $CI_REGISTRY_IMAGE:web
    - docker images

# deploy_staging:
#   stage: deploy_staging
#   type: deploy
#   environment:
#     name: staging
#     url: relay.sellan.fr
#   script:
#     - echo "$sshkey" > /key
#     - chmod 400 /key
#     - ssh -i /key -tt soulei@relay.sellan.fr "sudo docker pull registry.gitlab.com/souleiseg/docker_ci-cd:web && sudo docker pull registry.gitlab.com/souleiseg/docker_ci-cd_db:db && sudo docker run -d -p 3306:3306 registry.gitlab.com/souleiseg/docker_ci-cd_db:db --default-authentication-plugin=mysql_native_password && sudo docker run -d -p 80:3000 registry.gitlab.com/souleiseg/docker_ci-cd:web && exit"
#   only:
#     - master   
```
Nous allons retrouver les 4 étapes de notre pipeline d'intégration : lint, test, build, deploy.

> Note : la dernière étape est commenté car nous n'avons pas encore réussi à déployer l'application. Nous nous sommes retrouvés avec des problèmes d'authentification (ssh mais aussi vers le registry gitlab pour récupérer les images), des problème d'architecture (au début nous avons voulu faire le déploiement sur une raspberry qui se base sur ARMv8) et finalement un problème de communication entre nos containers sur `relay.sellan.fr` (problème non rencontré en local).

### Configuration de la base de données
- Dockerfile :
```(dockerfile)
FROM mysql
EXPOSE 3306/tcp
WORKDIR /var/lib/mysql
ENV MYSQL_ROOT_PASSWORD=root
ENV MYSQL_DATABASE=cicd
ENV MYSQL_USER=flocasia
ENV MYSQL_PASSWORD=redfall
ADD schema.sql /docker-entrypoint-initdb.d
```
Ce Dockerfile va permettre d'ouvrir le port 3306, de configurer nos variables d'environnement mysql et d'ajouter notre table "users" dans la base de données à l'aide du fichier `schema.sql`.

- Fichier `schema.sql`
```(sql)
DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `age` int DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

LOCK TABLES `users` WRITE;

UNLOCK TABLES;
```
Le fichier schema.sql est utilisé par le Dockerfile afin de créer une table users lorsque le conteneur est généré.

- Fichier `.gitlab-ci.yml` : 
```(yaml)
image: docker:18

stages:
  - build

services:
  - docker:dind

before_script:
  - echo -n $CI_JOB_TOKEN | docker login -u gitlab-ci-token --password-stdin $CI_REGISTRY

Build:
  stage: build
  script:
    - docker build -t $CI_REGISTRY_IMAGE:db ./
    - docker push $CI_REGISTRY_IMAGE:db
```
Ce fichier nous permet uniquement de pousser l'image de notre base de données sur le registry de gitlab.

## Guide du nouvel arrivant
### Utilisation de la pipeline d'intégration
#### Lancement de la pipeline d'intégration
Il vous faut au préalable avoir accès au repository git du projet.

Effectuer les modifications souhaités sur l'application web et envoyer ces dernières sur le repository git : 

```
$ git add <les fichiers à ajouter>
$ git commit -m "Message du commit"
$ git push
```

#### Suivre l'intégration
A partir du moment où vos modifications sont envoyés, la pipeline d'intégration va automatiquement se lancer. Vous pouvez suivre la pipeline [ici](https://gitlab.com/souleiseg/docker_ci-cd/-/pipelines).

![Liste des jobs](https://i.imgur.com/E19HFDv.png)

#### Résultats
Dans le cas où une étape echoue, vous pouvez regarder le déroulement de cette dernière en détail (sur le lien donné précédemment, il suffit de cliquer sur la pipeline correspondante, puis le job correspondant à l'étape).

Dans le cas d'un problème de lint ou de test, vous êtes invités à regarder le résultat du job pour savoir quel est le problème (qui doit être dû aux dernières modifications). Si le problème vient du déploiement, vous êtes invités à vérifier l'état du serveur.

Exemple d'un fail de lint : 
![Lint job fail](https://i.imgur.com/4eyfy33.png)

Exemple d'un fail de test : 
![Lint test fail](https://i.imgur.com/gGTvi3Z.png)

Une fois les quatres étapes de la pipeline terminées et valides, il suffit de se rendre à l'adresse du serveur de test (port 80). Vous pouvez directement tester vos modifications.

### Lancer l'application
Malgré le fait que l'application ne se déploie pas à cause des difficultés rencontrées, il est tout de même possible de la tester en local : 
```
$ docker run -d -p 80:3000 registry.gitlab.com/souleiseg/docker_ci-cd:web
$ docker run -d -p 3306:3306 registry.gitlab.com/souleiseg/docker_ci-cd_db:db --default-authentication-plugin=mysql_native_password 
```

> Option -d : Les conteneurs démarrent en mode detach et tournent donc en arrière plan.
> Option -p : Le port 80 de votre machine est lié au port 3000 du container web.

Le container MySQL généré à ce moment, intègre une base de données "cicd" possédant une tables "users" :

![Tableau BDD](https://i.imgur.com/hTKXnDH.png)

### Utiliser l'application web
Voici les routes existantes :
• `GET /` (Page d'accueil) 
• `GET /users` (Afficher tous les utilisateurs)
• `GET /users/<userID>` (Afficher un utilisateur selon son ID)

Insérer un utilisateur via Postman :
![](https://i.imgur.com/7EMdoln.png)